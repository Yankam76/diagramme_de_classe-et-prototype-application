import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

public class AfficherRC extends JFrame implements ActionListener{
	
private static final long serialVersionUID = 1L;
	
	private JPanel containerPanel;

	private JLabel labelAfficherReservationClient ;
	private JLabel labelEspace;
	
	private JLabel NomClient;
			
	private JTextField TextNomClient;
	
	
	private JButton Valider;
	
	/**
	 *  bouton qui permet de revenir au menu precedent
	 */
	private JButton Precedent;	
	
	/**
	 * Zone de texte pour afficher les Tables
	 */
	JTextArea zoneTextReservation;
	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;
	/**
	 * instance de ArticleDAO permettant les acc�s � la base de donn�es
	 */
	
	public AfficherRC ()
	{
				// on nomme la fenetre
				this.setTitle("AFFICHAGE DES RESERVATION PAR CLIENT");
						
				//on gere la taille
						
				this.setSize(400, 340);
						
				//Nous demandons maintenant � notre objet de se positionner au centre
				this.setLocationRelativeTo(null);
				
				//cr�ation du conteneur
				containerPanel = new JPanel();
				
				//choix du Layout pour ce conteneur
				//il permet de g�rer la position des �l�ments
				//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
				containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
				
				//choix de la couleur pour le conteneur
		        containerPanel.setBackground(Color.GREEN);
		        
		        
		        labelAfficherReservationClient =new JLabel("                   AFFICHAGE DES RESERVATION PAR CLIENT            ");
		        
		        labelEspace = new JLabel ("________________________________________________");
				        		        		        		        		     
		        NomClient = new JLabel(" NOM DU CLIENT");
		        
		        TextNomClient = new JTextField();

		        Valider = new JButton (" VALIDER ");
		        
		        Precedent= new JButton (" PRECEDENT ");

		        zoneTextReservation = new JTextArea(5, 20);
		        zoneDefilement = new JScrollPane(zoneTextReservation); 
		        zoneTextReservation.setEditable(false);
		        
		        
		        //TITRE
		        
		        containerPanel.add(labelAfficherReservationClient);
		        containerPanel.add(labelEspace);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
 
		        //Client
		        
		        containerPanel.add(NomClient);
		        containerPanel.add(TextNomClient);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //VALIDER
		        
		        containerPanel.add(Valider);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //Affichage
		        
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
				containerPanel.add(zoneDefilement);
				
				//Precedent
				
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
				containerPanel.add(Precedent);
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));

		        
		        //ajouter une bordure vide de taille constante autour de l'ensemble des composants
				containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				
				Valider.addActionListener(this);
				Precedent.addActionListener(this);
				 
				 
				//Termine le processus lorsqu'on clique sur la croix rouge
			    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			    this.setContentPane(containerPanel);
				
				this.setVisible(true);
		}
				
				public void actionPerformed(ActionEvent action)
				{
					if(action.getSource()== Precedent)
					{
						this.setVisible(false);
						new EspaceReservation();
					}
				}
		        		      
		        
		        
		        
		        
		
	}
	
