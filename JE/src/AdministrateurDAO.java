import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AdministrateurDAO {
	
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN="YANKAM";
	final static String PASS="YANKAM";
	
	public AdministrateurDAO(){
		// chargement du pilote de bases de donn�es
		try {
				Class.forName("oracle.jdbc.OracleDriver");
			} catch (ClassNotFoundException e2) {
				System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}
	}
	
	/**
	 * Permet de rechercher un identifiant et un mot de passe dans la base de donn�es
	 * @param identifiant
	 * @param motDePasse
	 * @return administrateur
	 */
	public Administrateur connexion(String login, String password){
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Administrateur admin=null;
		
		//connexion � la base de donn�es
		try {
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Gestionnaire WHERE GES_LOGIN = ? AND GES_PASSWORD = ?");
				ps.setString(1,login);
				ps.setString(2,password);

				//on ex�cute la requ�te
				//rs contient un pointeur situ� jusute avant la premi�re ligne retourn�e
				rs=ps.executeQuery();
				//passe � la premi�re (et unique) ligne retourn�e 
				if(rs.next()){
					admin = new Administrateur(rs.getString("GES_LOGIN"),rs.getString("GES_PASSWORD"));
					System.out.println("Administrateur trouv�");
					
				}

			} 
		catch (Exception ee) {
					ee.printStackTrace();
		}
		
		finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		
		return admin;
		
	}

}
