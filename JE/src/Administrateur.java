
public class Administrateur {
	
	private String password;
	private String login;
	
	public Administrateur(String login, String password){
		
		this.password=password;
		this.login=login;
	}
	
	public String getLogin()
	{
		return login;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String pass)
	{
		password = pass ;
	}
	
	public void setLogin(String login)
	{
		this.login = login;
	}

}
