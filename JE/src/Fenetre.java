 import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;


public class Fenetre extends JFrame implements ActionListener{
	
	/**
	 * numero de version pour classe serialisable
	 * Permet d'eviter le warning "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L; 
	
	/**
	 * conteneur : il accueille les differents composants graphiques de MaFenetre
	 */
	private JPanel containerPanel;
	
	/**
	 * zone de texte pour le login
	 */
	private JTextField textFieldLogin;
	
	/**
	 * zone de texte pour le mot de passe
	 */
	private JTextField textFieldPassword;
	
	/**
	 * label designation
	 */
	private JLabel labelLogin;
	
	/**
	 * label Password
	 */
	private JLabel labelPassword;
	
	/**
	 * bouton de connexion
	 */
	private JButton boutonConnexion;
	
	/**
	 *  bouton qui permet de revenir au menu precedent
	 */
	private JButton Precedent;
	
	
	public Fenetre(){
		
		// on nomme la fenetre
		this.setTitle("CONNEXION");
				
		//on gere la taille
				
		this.setSize(400, 210);
				
		//Nous demandons maintenant � notre objet de se positionner au centre
		this.setLocationRelativeTo(null);
		
		//cr�ation du conteneur
		containerPanel = new JPanel();
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.GREEN);
        
      //instantiation des  composants graphiques
      textFieldLogin=new JTextField();
      		
      textFieldPassword= new JTextField();
      		
      boutonConnexion=new JButton("CONNEXION");
      		
      Precedent= new JButton (" PRECEDENT ");
      		
      labelLogin=new JLabel("LOGIN :");
      		
      labelPassword = new JLabel (" PASSWORD :");
      
      
      
    //ajout des composants sur le container 
		
		
		//Login	
		containerPanel.add(labelLogin);
		//introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(textFieldLogin);
		//introduire une espace constant entre le champ texte et le composant suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
		
		//Password
		containerPanel.add(labelPassword);
		containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
		containerPanel.add(textFieldPassword);
		containerPanel.add(Box.createRigidArea(new Dimension(0,10)));
		
		//Connexion
		containerPanel.add(boutonConnexion);
		containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
 
		
		//Precedent		
		
		containerPanel.add(Precedent);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
 
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		
		Precedent.addActionListener(this);
		boutonConnexion.addActionListener(this);
				
		//Termine le processus lorsqu'on clique sur la croix rouge
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setContentPane(containerPanel);
		
		this.setVisible(true);
	
	}
	
	
	
	

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		
		if(ae.getSource()==boutonConnexion)
		{
			Administrateur admin;
			
			
			AdministrateurDAO monAdmin = new AdministrateurDAO();
			
			admin = monAdmin.connexion(this.textFieldLogin.getText(), this.textFieldPassword.getText());
			
			if(admin == null)
				System.out.println("Cet administrateur n'existe pas");
			else
			{
				new EspaceG();
			
			this.setVisible(false);
			
			}		
		}
		
		if(ae.getSource()== Precedent)
		{
			this.setVisible(false);
			new KEVIN();
		}
	}
	
}
