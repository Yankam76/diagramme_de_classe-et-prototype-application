import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;


public class CreerTable extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private JPanel containerPanel;
	
	private JLabel labelCreerTable;
	private JLabel labelEspace;
	private JLabel Numero;
	private JLabel NombrePlace;
	/**
	 *  bouton qui permet de revenir au menu precedent
	 */
	private JButton Precedent;
	
	private JButton Valider;
	
	private TableDAO MaTableDAO;
	
	
	
	/**
	 * bouton de EspaceGestionnaire
	 */
	private JTextField textFieldNumero;
	private JTextField textFielNombre;
	
	public CreerTable()
	{
				this.MaTableDAO = new TableDAO();
		
		        // on nomme la fenetre
				this.setTitle("CREER TABLE");
						
				//on gere la taille
						
				this.setSize(400, 320);
						
				//Nous demandons maintenant � notre objet de se positionner au centre
				this.setLocationRelativeTo(null);
				
				//cr�ation du conteneur
				containerPanel = new JPanel();
				
				//choix du Layout pour ce conteneur
				//il permet de g�rer la position des �l�ments
				//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
				containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
				
				//choix de la couleur pour le conteneur
		        containerPanel.setBackground(Color.YELLOW);
		        
		        
		        labelCreerTable =new JLabel("                   CREER TABLE            ");
		        
		        labelEspace = new JLabel ("__________________________________");
		        
		        Numero = new JLabel (" NUMERO TABLE");
		        
		        NombrePlace = new JLabel (" NOMBRE DE PLACE");
		        
		        textFieldNumero = new JTextField ();
		        
		        textFielNombre = new JTextField ();
		        
		        Valider = new JButton ("     VALIDER    ");
		        
		        Precedent= new JButton (" PRECEDENT ");
		        
		        //creer table
		        
		        containerPanel.add(labelCreerTable);
		        containerPanel.add(labelEspace);
		        containerPanel.add(Numero);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //textField
		        
		        containerPanel.add(textFieldNumero);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //
		        containerPanel.add(NombrePlace);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //textFiel
		        containerPanel.add(textFielNombre);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        
		      //Valider
		        containerPanel.add(Valider);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		      //Precedent		
				
				containerPanel.add(Precedent);
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        
		      //ajouter une bordure vide de taille constante autour de l'ensemble des composants
				containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				
				Valider.addActionListener(this);
				Precedent.addActionListener(this);
				
				//Termine le processus lorsqu'on clique sur la croix rouge
			    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			    this.setContentPane(containerPanel);
				
				this.setVisible(true);
	}
				
				public void actionPerformed(ActionEvent action)
				{
					int retour; // code de retour de la classe ArticleDAO
					
					if(action.getSource()== Precedent)
					{
						this.setVisible(false);
						new EspaceG();
					}
					
					
					
					
						if(action.getSource()==Valider)
						{
							//on cr�e l'objet message
							Table a=new Table(this.textFieldNumero.getText(), Integer.parseInt(this.textFielNombre.getText()));
							//on demande � la classe de communication d'envoyer l'article dans la table article
							retour = MaTableDAO.ajouter(a);
							// affichage du nombre de lignes ajout�es
							// dans la bdd pour v�rification
							System.out.println("" + retour + " ligne ajout�e ");
						}
				}
		        
		        

		
		        
		        
		
		
	}
	
	
	

	
	
	
	
	
	

