
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;


 
public class KEVIN extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * conteneur : il accueille les differents composants graphiques de MaFenetre
	 */
	private JPanel containerPanel;
	
	private JLabel Way;
	private JLabel hum;

	/**
	 * bouton de EspaceGestionnaire
	 */
	private JButton boutonEspaceGestionnaire;
	
	/**
	 * bouton pour boutonEspaceReservation
	 */
	private JButton boutonEspaceReservation;
	
	/**
	 * bouton pour quitter
	 */
	private JButton boutonQuitter;
	
	
	
	public KEVIN() {
		// TODO Auto-generated constructor stub
		
		
				// on nomme la fenetre
				this.setTitle("ESPACE GESTION/RESERVATION");
				
				
				//on gere la taille
				
				this.setSize(400, 210);
				
				//Nous demandons maintenant � notre objet de se positionner au centre
				this.setLocationRelativeTo(null);
				
				//cr�ation du conteneur
				containerPanel = new JPanel();
				
				//choix du Layout pour ce conteneur
				//il permet de g�rer la position des �l�ments
				//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
				//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
				containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
				
				//choix de la couleur pour le conteneur
		        containerPanel.setBackground(Color.GRAY);
		        //containerPanel.setBackground(Color.BLUE);
		        
		        boutonEspaceGestionnaire=new JButton("ESPACE GESTIONNAIRE");
		        
		        boutonEspaceReservation=new JButton("RESERVATION");
		        
		        Way =new JLabel("                   MENU               ");
		        
		        hum = new JLabel ("___________________________________________");
		        
		        boutonQuitter=new JButton("QUITTER");
		        
		        //Menu
		        
		        containerPanel.add(Way);
		        containerPanel.add(hum);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        
		        
		        //Connexion
				containerPanel.add(boutonEspaceGestionnaire);
				containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
		 
				
			    //ESPACE RESERVATION
				containerPanel.add(boutonEspaceReservation);
				containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
				
				//Quitter
				containerPanel.add(boutonQuitter);
				containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
				
				//ajouter une bordure vide de taille constante autour de l'ensemble des composants
				containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				
				
				boutonEspaceReservation.addActionListener(this);
				boutonEspaceGestionnaire.addActionListener(this);
				boutonQuitter.addActionListener(this);
				
				//Termine le processus lorsqu'on clique sur la croix rouge
			    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			    this.setContentPane(containerPanel);
				
				this.setVisible(true);
				
					
		
	}


	@Override
	public void actionPerformed(ActionEvent action) {
		if(action.getSource()==boutonEspaceGestionnaire){
			this.setVisible(false);
			 new Fenetre();
		}
		//EspaceReservation
        
        if(action.getSource()==boutonEspaceReservation)
        {
            this.setVisible(false);
             new EspaceReservation();
           
        }
		
        if(action.getSource()==boutonQuitter)
		{
			this.setVisible(false);
			this.dispose();
		}
	}

}
