import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

public class VerifierD extends JFrame implements ActionListener{
	
private static final long serialVersionUID = 1L;
	
	private JPanel containerPanel;

	private JLabel labelVerifierDisponibilite ;
	private JLabel labelEspace;
	
	private JLabel DateReservation;
	
	private JLabel Service;
	
	
	private JTextField DateR;
	
	private JTextField Serv;
	
	private JButton Valider;
	
	/**
	 *  bouton qui permet d'afficher tous les articles
	 */
	private JButton Afficher;	
	
	/**
	 *  bouton qui permet de revenir au menu precedent
	 */
	private JButton Precedent;

	
	/**
	 * Zone de texte pour afficher les Tables
	 */
	JTextArea zoneTextTable;
	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;
	/**
	 * instance de ArticleDAO permettant les acc�s � la base de donn�es
	 */
	
	public VerifierD ()
	{
				// on nomme la fenetre
				this.setTitle("VERIFIER DISPONIBILITE");
						
				//on gere la taille
						
				this.setSize(400, 360);
						
				//Nous demandons maintenant � notre objet de se positionner au centre
				this.setLocationRelativeTo(null);
				
				//cr�ation du conteneur
				containerPanel = new JPanel();
				
				//choix du Layout pour ce conteneur
				//il permet de g�rer la position des �l�ments
				//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
				containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
				
				//choix de la couleur pour le conteneur
		        containerPanel.setBackground(Color.GREEN);
		        
		        
		        labelVerifierDisponibilite =new JLabel("                   VERIFIER DISPONIBILITE            ");
		        
		        labelEspace = new JLabel ("__________________________________");
		        
		        
		        
		        DateReservation = new JLabel (" DATE DE RESERVATION ");
		        
		        
		        
		        Service = new JLabel (" Service (midi/soir)  ");
		        
		     
		        DateR = new JTextField();
		        
		        Precedent= new JButton (" PRECEDENT ");
		        
		        Serv = new JTextField();
		        
		        Valider = new JButton (" VALIDER ");
		        
		        Afficher = new JButton (" AFFICHER TABLE LIBRE ");
		        
		        zoneTextTable = new JTextArea(5, 20);
		        zoneDefilement = new JScrollPane(zoneTextTable); 
		        zoneTextTable.setEditable(false);
		        
		        
		        //Verifier Disponibilite
		        
		        containerPanel.add(labelVerifierDisponibilite);
		        containerPanel.add(labelEspace);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		       
		        //Date
		        
		        containerPanel.add(DateReservation);
		        containerPanel.add(DateR);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		       
		        
		        //Service
		        
		        containerPanel.add(Service);
		        containerPanel.add(Serv);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //VALIDER
		        
		        containerPanel.add(Valider);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //Verifier Disponibilit�     
		        containerPanel.add(Afficher);
		        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        //Affichage
		        
				containerPanel.add(Box.createRigidArea(new Dimension(0,5)));
				containerPanel.add(zoneDefilement);
		        
				//Precedent		
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
				containerPanel.add(Precedent);
				containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		        
		        
		        
		        //ajouter une bordure vide de taille constante autour de l'ensemble des composants
				containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				
				Valider.addActionListener(this);
				Afficher.addActionListener(this);
				Precedent.addActionListener(this);
				
				 
				 
				//Termine le processus lorsqu'on clique sur la croix rouge
			    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			    this.setContentPane(containerPanel);
				
				this.setVisible(true);
		}
				
				public void actionPerformed(ActionEvent action)
				{
					if(action.getSource()== Precedent)
					{
						this.setVisible(false);
						new EspaceReservation();
					}
				}
		        		      
		        
		        
		        
		        
		
	}
	
