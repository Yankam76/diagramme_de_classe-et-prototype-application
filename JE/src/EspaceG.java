import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;



public class EspaceG extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * conteneur : il accueille les differents composants graphiques de EspaceG
	 */
	private JPanel containerPanel;
	
	/**
	 * bouton de boutonCreerTable;
	 */
	private JButton boutonCreerTable;
	
	/**
	 * bouton de boutonSupprimerTable;
	 */
	private JButton boutonSupprimerTable;
	
	/**
	 * bouton de boutonSupprimerTable;
	 */
	private JButton boutonEspaceReservation;
	
	
	public EspaceG ()
	{
		// TODO Auto-generated constructor stub
		
		
		// on nomme la fenetre
		this.setTitle("ESPACE GESTION");
		
		//on gere la taille
		
		this.setSize(400, 150);
		
		//Nous demandons maintenant � notre objet de se positionner au centre
		this.setLocationRelativeTo(null);
		
		//cr�ation du conteneur
		containerPanel = new JPanel();
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
		
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.GRAY);
        
        boutonCreerTable=new JButton("CREER TABLE");
        
        boutonSupprimerTable=new JButton("SUPPRIMER TABLE");
        
        boutonEspaceReservation=new JButton("RESERVATION");
        
        
        //CREER TABLE 
		containerPanel.add(boutonCreerTable);
		containerPanel.add(Box.createRigidArea(new Dimension(5,5)));

		//SUPPRIMER TABLE 
		containerPanel.add(boutonSupprimerTable);
		containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
		
		//GESTION 
		containerPanel.add(boutonEspaceReservation);
		containerPanel.add(Box.createRigidArea(new Dimension(5,5)));
		
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		 boutonCreerTable.addActionListener(this);
		
		 boutonSupprimerTable.addActionListener(this);
		 
		 boutonEspaceReservation.addActionListener(this);
		 
		 
		//Termine le processus lorsqu'on clique sur la croix rouge
		    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		    this.setContentPane(containerPanel);
			
			this.setVisible(true);

		
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		
		if(action.getSource()==boutonCreerTable)
		{
			this.setVisible(false);
			new CreerTable();
		}
		
		if(action.getSource()==boutonSupprimerTable)
		{
			this.setVisible(false);
			 new SupprimerTable();
		}
		
		//EspaceReservation
       
        if(action.getSource()==boutonEspaceReservation)
        {
            this.setVisible(false);
            new EspaceReservation();
            
        }
        
        
	}

}
	
	
	
	
	

	
	
	
	

