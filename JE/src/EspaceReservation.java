import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;


public class EspaceReservation extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * conteneur : il accueille les differents composants graphiques de EspaceG
	 */
	private JPanel containerPanel;
	
	/**
	 * bouton de boutonCreerTable
	 */
	private JButton boutonVerifDispo;
	
	/**
	 * bouton de boutonSupprimerTable
	 */
	private JButton boutonCreerReservation;
	
	/**
	 * bouton de boutonSupprimerTable
	 */
	private JButton boutonAnnulerReservation;
	
	/**
	 * bouton de boutonAfficherReservationClient
	 */
	private JButton boutonAfficherReservationClient;
	
	/**
	 * bouton de boutonAfficherReservationService
	 */
	private JButton boutonAfficherReservationService;
	
	/**
	 *  bouton qui permet de revenir au menu precedent
	 */
	private JButton Precedent;
	
	private JLabel labelRESERVATION;
	
	private JLabel labelEspace;
	
	
	public EspaceReservation ()
	{
		// TODO Auto-generated constructor stub
		
		
		// on nomme la fenetre
		this.setTitle("ESPACE RESERVATION");
		
		//on gere la taille
		
		this.setSize(400, 360);
		
		//Nous demandons maintenant � notre objet de se positionner au centre
		this.setLocationRelativeTo(null);
		
		//cr�ation du conteneur
		containerPanel = new JPanel();
		
		//choix du Layout pour ce conteneur
		//il permet de g�rer la position des �l�ments
		//il autorisera un retaillage de la fen�tre en conservant la pr�sentation
		//BoxLayout permet par exemple de positionner les �lements sur une colonne ( PAGE_AXIS )
		
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		
		
		//choix de la couleur pour le conteneur
        containerPanel.setBackground(Color.GRAY);
        
        labelRESERVATION =new JLabel("                   ESPACE RESERVATION            ");
        
        labelEspace = new JLabel ("__________________________________");
        
        boutonVerifDispo=new JButton("Verification de la disponibilit�");
        
        boutonCreerReservation=new JButton("Creer une reservation");
        
        boutonAnnulerReservation=new JButton("Annuler une r�servation");
        
        boutonAfficherReservationClient=new JButton("Afficher r�servation client");
        
        boutonAfficherReservationService=new JButton("Afficher une r�servation service");
        
        Precedent= new JButton (" MENU PRINCIPAL ");
        
        //TITRE
        
        containerPanel.add(labelRESERVATION);
        containerPanel.add(labelEspace);
        
        containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
        
        //Verifier une disponibilité
		containerPanel.add(boutonVerifDispo);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));

		//Creer une reservation
		containerPanel.add(boutonCreerReservation);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		
		//Annuler une reservation
		containerPanel.add(boutonAnnulerReservation);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		
		//Afficher une reservation client
		containerPanel.add(boutonAfficherReservationClient);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		
		//Afficher une reservation service
		containerPanel.add(boutonAfficherReservationService);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		
		//Precedent		
		
		containerPanel.add(Precedent);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		 
		
		//ajouter une bordure vide de taille constante autour de l'ensemble des composants
		containerPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		boutonVerifDispo.addActionListener(this);
		
		boutonCreerReservation.addActionListener(this);
		 
		boutonAnnulerReservation.addActionListener(this);
		 
		boutonAfficherReservationService.addActionListener(this);
		
		boutonAfficherReservationClient.addActionListener(this);
		
		Precedent.addActionListener(this);
		 
		//Termine le processus lorsqu'on clique sur la croix rouge
		    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		    this.setContentPane(containerPanel);
			
			this.setVisible(true);

		
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		
		if(action.getSource()==boutonCreerReservation)
		{
			this.setVisible(false);
			new CreerR();
		}
		
		if(action.getSource()==boutonAnnulerReservation)
		{
			this.setVisible(false);
			new AnnulerR();
		}
		
		if(action.getSource()==boutonVerifDispo)
        {
        	this.setVisible(false);
        	new VerifierD();
        }
		
		if(action.getSource()==boutonAfficherReservationService)
		{
			this.setVisible(false);
			new AfficherRS();
		}
		
		if(action.getSource()==boutonAfficherReservationClient)
		{
			this.setVisible(false);
			new AfficherRC();
		}
		
		if(action.getSource()== Precedent)
		{
			this.setVisible(false);
			new KEVIN();
		}
	}

}
	
	
	
	
	

	
	
	
	

