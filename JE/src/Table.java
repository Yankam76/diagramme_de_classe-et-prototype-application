
public class Table {

	private String Numero;
	private int Nombre;
	
	public Table(String Numero,int Nombre)
	{
		this.Numero = Numero;
		
		this.Nombre = Nombre;
	}
	
	public String getNumero()
	{
		return Numero;
		
	}
	
	public int getNombre()
	{
		
		return Nombre;
	}
	
	public void setNombre(int n)
	{
		Nombre = n;
	}
	
	public void setNumero(String N)
	{
		Numero = N;
	}
	
	
	
}
